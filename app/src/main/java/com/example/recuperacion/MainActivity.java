package com.example.recuperacion;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText txtUser;
    private EditText txtPass;
    private Button btnIngresar;
    private Button btnSalir;
    private Button btnLim;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtUser = (EditText) findViewById(R.id.txtUsuario);
        txtPass = (EditText) findViewById(R.id.txtContra);
        btnIngresar = (Button) findViewById(R.id.btnEntrar);
        btnSalir = (Button) findViewById(R.id.btnSalir);
        btnLim = (Button) findViewById(R.id.btnLimp);

        final String stringuser = getString(R.string.user);
        final String stringpass = getString(R.string.pass);

        btnIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String usuario = txtUser.getText().toString();
                String contra = txtPass.getText().toString();
                if(usuario.matches("")){
                    Toast.makeText(MainActivity.this,"Se debe de ingresar un usuario",Toast.LENGTH_SHORT).show();
                }
                else if(contra.matches("")){
                    Toast.makeText(MainActivity.this,"Se debe de ingresar una contraseña",Toast.LENGTH_SHORT).show();
                }
                else{
                    if (usuario.matches(stringuser) && contra.matches(stringpass)) {
                        Intent i = new Intent(MainActivity.this, CalculadoraActivity.class);
                        i.putExtra("usuario", usuario);

                        startActivity(i);
                    }else{
                        Toast.makeText(MainActivity.this,"Datos incorrectos",Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        btnLim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtUser.setText("");
                txtPass.setText("");
            }
        });

        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }
}
